﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class Question 
{
    public string question = "";
}

[Serializable]
public class SurveyQuestion : Question
{
    [Serializable]
    public class QuestionAnsweredEvent : UnityEvent<Answer, string>
    {

    }

    public List<string> answers = new List<string>();
    public QuestionAnsweredEvent OnQuestionAnswered = new QuestionAnsweredEvent();
}

[Serializable]
public class ScenarioQuestion : Question
{
    public string animationTrigger = "";
    public AudioClip voiceClip = null;
    public List<AnswerPair> answerPairs = new List<AnswerPair>();
}

[Serializable]
public class ScenarioNode : Question
{
    // Opening
    public string additionalQuestion = "";
    public string animationTrigger = "";
    public AudioClip voiceClip = null;

    // The answers shown to the opener, then the corresponding bully response
    public AnswerPair[] answerPairs = new AnswerPair[3];

    // Another exchange that has the reference to the next node
    public AnswerHook[] answerHooks = new AnswerHook[3];
}

[Serializable]
public class AnswerPair
{
    [TextArea]
    public string answer = "";
    [TextArea]
    public string response = "";

    public string animationTrigger = "";
    public AudioClip voiceClip = null;
}

[Serializable]
public class AnswerHook : AnswerPair
{
    public ScenarioNodeData node;
}
