﻿using System;

[Serializable]
public class Answer
{
    public string race = "No answer";
    public string gender = "No answer";
    public string sensitivity = "No answer";
    public string family = "No answer";
    public string drugs = "No answer";
    public string discrimination = "No answer";
    public string alone = "No answer";
    public string friends = "No answer";
    public string freeMeals = "No answer";
    public string depressed = "No answer";

    public Answer()
    {
        // Empty
    }
}
