using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool isMale;
    public GameObject playerCharacter;
    public GameObject male;
    public GameObject female;

    public void genderSetting(){


        if(isMale){

            playerCharacter = male;
            playerCharacter.SetActive(true);
           
        }

         if(!isMale){

            playerCharacter = female;
            playerCharacter.SetActive(true);
         

    }
   
}
}