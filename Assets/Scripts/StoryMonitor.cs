﻿using NodeCanvas.StateMachines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class StoryMonitor : MonoBehaviour
{
    public bool AlarmTurnedOff;
    public bool CurtainLooked;
    public bool CupboardOpened;
    public bool NiqabWorn;
    public bool MirrorLooked;
    public XRGrabInteractable interactable1;
    public XRGrabInteractable interactable2;
    public XRBaseControllerInteractor controller;
    public bool selectTest1;
    public bool selectTest2;
    public  GameObject experimentManager;


    // Start is called before the first frame update
    void Start()
    {
        AlarmTurnedOff = false;
        CurtainLooked = false;
        CupboardOpened = false;
        NiqabWorn = false;
        MirrorLooked = false;
        selectTest1 = false;
        selectTest2= false;
       


    }

    // Update is called once per frame
    void Update()
    {
        
        if (selectTest1)
            {
            selectTest1 = false;
            interactable1.onSelectEntered.Invoke(controller);
            }

        if (selectTest2)
        {
            selectTest2 = false;
            interactable2.onSelectEntered.Invoke(controller);
            experimentManager.GetComponent<FSMOwner>().StartBehaviour();
        }
    }


    public void AlarmWasTurnedOff()
    {
        AlarmTurnedOff = true;
       
    }

    public void CurtainWasLookedAt()
    {
        CurtainLooked = true;
    }

    public void CupboardWasOpened()
    {
        CupboardOpened = true;
    }

    public void NiqabWasWorn()
    {
        NiqabWorn = true;
    }
   public void MirrorWasLookedAt()
    {
        MirrorLooked = true;
    }
}
