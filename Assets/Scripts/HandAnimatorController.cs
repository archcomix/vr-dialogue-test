﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandAnimatorController : MonoBehaviour
{

	public InputDeviceCharacteristics controllerCharacteristics;
    public Animator animator;
    public GameObject character;
    public bool isLeft;
	private InputDevice device;

	
    // Start is called before the first frame update
    void Start()
    {
        character = gameObject.GetComponentInParent<Player>().playerCharacter;
        animator = character.GetComponent<Animator>();
        
    	TryInitialize();

    }

   // Update is called once per frame
    void Update()
    {

        if (!device.isValid)
        {

            TryInitialize();         

        }

        if (device.isValid)
        {

         //   if(character.active) 
         //   {

                if(isLeft)
                {

                    CheckLeftHandTrigger();
                    CheckLeftHandGrip();

                }

                if(!isLeft)
                {

                    CheckRightHandTrigger();
                    CheckRightHandGrip();

                }
        //    }

        }

    }

    public void CheckRightHandTrigger(){

        if (device.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            animator.SetFloat("indexRight", triggerValue);
           // Debug.Log("Index Right: "+ triggerValue );
        } else{

            animator.SetFloat("indexRight", 0.0f);

        }

    }

    public void CheckLeftHandTrigger(){

        if (device.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            animator.SetFloat("indexLeft", triggerValue);
         //   Debug.Log("Index Left: "+ triggerValue );
        } else{

           animator.SetFloat("indexLeft", 0.0f);

        }

    }

    public void CheckRightHandGrip(){

        if (device.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            animator.SetFloat("fingersRight", gripValue);
           // Debug.Log("Fingers Right: "+gripValue );
        } else{

            animator.SetFloat("fingersRight", 0.0f);

        }

    }

     public void CheckLeftHandGrip(){

        if (device.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            animator.SetFloat("fingersLeft", gripValue);
          //  Debug.Log("Fingers Left: "+ gripValue );
        } else{

            animator.SetFloat("fingersLeft", 0.0f);

        }

    }

    void TryInitialize()
    {

        List<InputDevice> devices = new List<InputDevice>();

        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);

        foreach (var item in devices)
        {

            Debug.Log(item.name + item.characteristics);

        }

        if (devices.Count > 0)
        {

            device = devices[0];

        }

    }
}