﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LikertScaleController : MonoBehaviour
{
    private Slider slider;

    public Text minLabel;
    public Text maxLabel;

    public RectTransform numbers;
    public GameObject digit;

    private List<GameObject> digits;

    private void Awake()
    {
        slider = GetComponentInChildren<Slider>();
        digits = new List<GameObject>();
        digits.Add(digit);
    }

    public void ResetValue()
    {
        slider.value = slider.minValue;
    }

    public int Value
    {
        get
        {
            return (int)slider.value;
        }
    }

    public void SetScaleSize(int size)
    {
        slider.maxValue = size;

        foreach (var digit in digits)
        {
            digit.SetActive(false);
        }

        for (int i = 0; i <= size; i++)
        {
            if(digits.Count <= i)
            {
                var newdigit = GameObject.Instantiate(digit, numbers);
                digits.Add(newdigit);
            }

            digits[i].SetActive(true);
            digits[i].GetComponent<Text>().text = (i + 1).ToString();
            var rt = digits[i].transform as RectTransform;
            var p = rt.anchoredPosition;
            p.x = (i / (float)size) * numbers.rect.width;
            rt.anchoredPosition = p;
        }
    }

    public void SetMinLabel(string label)
    {
        minLabel.text = label;
    }

    public void SetMaxLabel(string label)
    {
        maxLabel.text = label;
    }
}
