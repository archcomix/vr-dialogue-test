﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SurveyData : ResilientData
{
    public List<SurveyQuestion> mQuestions = new List<SurveyQuestion>();

    public void SetGender(Answer answers, string answer)
    {
        Debug.Log("Gender set: " + answer);

        answers.gender = answer;
    }

    public void SetRace(Answer answers, string answer)
    {
        Debug.Log("Race set: " + answer);

        answers.race = answer;
    }

    public void SetSensitivity(Answer answers, string answer)
    {
        Debug.Log("Sensitivity set: " + answer);

        answers.sensitivity = answer;
    }

    public void SetFamily(Answer answers, string answer)
    {
        Debug.Log("Family set: " + answer);

        answers.family = answer;
    }

    public void SetDrugs(Answer answers, string answer)
    {
        Debug.Log("Drugs set: " + answer);

        answers.drugs = answer;
    }

    public void SetDiscrimination(Answer answers, string answer)
    {
        Debug.Log("Discrimination set: " + answer);

        answers.discrimination = answer;
    }

    public void SetAlone(Answer answers, string answer)
    {
        Debug.Log("Alone set: " + answer);

        answers.alone = answer;
    }

    public void SetFriends(Answer answers, string answer)
    {
        Debug.Log("Friends set: " + answer);

        answers.friends = answer;
    }

    public void SetFreeMeals(Answer answers, string answer)
    {
        Debug.Log("Free Meals set: " + answer);

        answers.freeMeals = answer;
    }

    public void SetDepressed(Answer answers, string answer)
    {
        Debug.Log("Dpressed set: " + answer);

        answers.depressed = answer;
    }

    /*
    [MenuItem("Assets/Create/SurveyData")]
    private static void CreateMyAsset()
    {
        SurveyData asset = CreateInstance<SurveyData>();

        AssetDatabase.CreateAsset(asset, "Assets/_Resilient/Data/SurveyData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    */
}