﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ScenarioData : ResilientData
{
    public List<ScenarioQuestion> mQuestions = new List<ScenarioQuestion>();

    /*
    [MenuItem("Assets/Create/ScenarioData")]
    private static void CreateMyAsset()
    {
        ScenarioData asset = CreateInstance<ScenarioData>();

        AssetDatabase.CreateAsset(asset, "Assets/_Resilient/Data/ScenarioData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    */
}

