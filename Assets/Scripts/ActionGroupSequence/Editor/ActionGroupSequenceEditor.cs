﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Assertions;
using System.Linq;

namespace GeneTherapyVR
{
    [CustomEditor(typeof(ActionGroupSequence))]
    public class ActionGroupSequenceEditor : Editor
    {

        #region Exposed Fields
        #endregion

        private bool showDuplicatesWarning = false;

        ActionGroupSequence _target;

        private void OnEnable()
        {
            _target = (ActionGroupSequence)target;
            CheckPriorityDuplicatesAndShowWarningIfNeeded();
        }

        public override void OnInspectorGUI()
        {
            _target = (ActionGroupSequence)target;

            if(GUILayout.Button("Sort by Priority")) {
                _target.Sort();
            }
            if(GUILayout.Button("Auto-assign Priorities")) {
                for (int i = 0; i < _target.actionGroups.Count; i++) {
                    ActionGroup ag = _target.actionGroups[i];
                    ag.priority = (uint)i;
                    _target.actionGroups[i] = ag;
                }
                EditorUtility.SetDirty(_target);
                CheckPriorityDuplicatesAndShowWarningIfNeeded();
            }

            if (showDuplicatesWarning) {
                EditorGUILayout.HelpBox("Duplicate priorities. Equal priorities means the order will not be deterministic.", MessageType.Warning);
            }
            EditorGUI.BeginChangeCheck();
            base.OnInspectorGUI();
            if(EditorGUI.EndChangeCheck()) {
                _target.Sort();
                // duplicate priorities warning
                CheckPriorityDuplicatesAndShowWarningIfNeeded();
                EditorUtility.SetDirty(_target);
            }

            if(GUILayout.Button(" + ", GUILayout.MinWidth(20f), GUILayout.MaxWidth(100f))) {
                _target.actionGroups.Add(new ActionGroup());
            }
        }

        void CheckPriorityDuplicatesAndShowWarningIfNeeded()
        {
            if (_target.actionGroups == null)
                return;
            showDuplicatesWarning = _target.actionGroups.GroupBy(x => x.priority).Any(g => g.Count() > 1);
        }

    }
}
