﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace GeneTherapyVR
{

    public class ActionGroupSequence : MonoBehaviour
    {
        public List<ActionGroup> actionGroups;
        [SerializeField] bool playOnStart = false;
        [SerializeField] bool playOnlyOnce = true; // if true, only the first time Play is called will work. Following calls to Play will have no effect
        private int numberOfPlays = 0;

        void Start()
        {
            
            if (playOnStart)
                Play();
        }

        public void Sort()
        {
            // Sort by priority
            List<ActionGroup> tmp = actionGroups;
            tmp.Sort((a, b) => {
                if (a.priority < b.priority) return -1;
                else if (a.priority > b.priority) return 1;
                else return 0;
                //return a.priority.CompareTo(b);
            });
            actionGroups = tmp;
        }

        public void Play()
        {
            if (playOnlyOnce && numberOfPlays > 0) {
                Debug.LogWarning("This ActionGroupSequence has played once and is set to play only once but something is still trying to Play.", this);
                return;
            }
            numberOfPlays++;
            StartCoroutine(Play_Coroutine());
        }

        IEnumerator Play_Coroutine()
        {
            for (int i = 0; i < actionGroups.Count; i++) {
                yield return new WaitForSeconds(actionGroups[i].delay);
                ActionGroup group = actionGroups[i];
                Assert.IsNotNull(group.actions);
                for (int a = 0; a < group.actions.Length; a++) {
                    if(group.actions[a] != null)
                        group.actions[a].Invoke();
                }
               // for (int pa = 0; pa < group.predefinedActions.Length; pa++) {
              //      ExecPredefinedAction(group.predefinedActions[pa]);
               // }
            }
        }

       /* private void ExecPredefinedAction(PREDEFINED_ACTION _pAction)
        {
            switch(_pAction) {
                case PREDEFINED_ACTION.LOAD_NEXT_SCENE:
                    SceneLoad.Instance.SwitchToNextScene();
                    break;
            }
        }
        */

    }

}
