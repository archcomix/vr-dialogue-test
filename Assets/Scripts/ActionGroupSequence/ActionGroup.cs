﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GeneTherapyVR
{
    public enum PREDEFINED_ACTION { NONE, LOAD_NEXT_SCENE };
    [System.Serializable]
    public struct ActionGroup
    {
        [Tooltip("optional")] public string name;
        public uint priority;
        public float delay; // seconds
        public UnityEvent[] actions;
        public PREDEFINED_ACTION[] predefinedActions;
    }

}
