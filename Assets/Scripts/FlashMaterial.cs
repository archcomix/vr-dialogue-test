﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashMaterial : MonoBehaviour
{
    public Material material;
    public Gradient colour;
    public float frequency;

    private Color original;

    // Start is called before the first frame update
    void Start()
    {
        original = material.color;
    }

    // Update is called once per frame
    void Update()
    {
        material.color = colour.Evaluate(Mathf.Sin(2 * Mathf.PI * frequency * Time.time));
    }

    private void OnDisable()
    {
        material.color = original;
    }
}
