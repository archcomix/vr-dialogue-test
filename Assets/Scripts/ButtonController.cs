﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;

public class ButtonController : MonoBehaviour

{
       static readonly Dictionary<string, InputFeatureUsage<bool>> availableButtons = new Dictionary<string, InputFeatureUsage<bool>>
    {
        {"triggerButton", CommonUsages.triggerButton },
           {"gripButton", CommonUsages.gripButton }
    
    };
    
    public enum ButtonOption
    {
        triggerButton,
        gripButton

    };

    [Tooltip("Input device role (left or right controller)")]
    public InputDeviceRole deviceRole;

    [Tooltip("Select the button")]
    public ButtonOption button;

    [Tooltip("Event when the button starts being pressed")]
    public UnityEvent OnPress;

    [Tooltip("Event when the button is released")]
    public UnityEvent OnRelease;

    //to check if it's being pressed
    public bool IsPressed { get; private set; }

    //to obtain input devices
    List<InputDevice> inputDevices;
    bool inputValue;

    InputFeatureUsage<bool> inputFeature;

    private void Awake()
    {
        string featureLabel = Enum.GetName(typeof(ButtonOption), button);

        availableButtons.TryGetValue(featureLabel, out inputFeature);

        inputDevices = new List<InputDevice>();
    }


    // Update is called once per frame
    void Update()
    {
        InputDevices.GetDevicesWithRole(deviceRole, inputDevices);
        for (int i = 0; i < inputDevices.Count; i++)
        {
            if(inputDevices[i].TryGetFeatureValue(inputFeature,out inputValue) && inputValue)
            {
                if (!IsPressed)
                {
                    IsPressed = true;
                    OnPress.Invoke();
                }
            }

            //check for button release

            else if (IsPressed)
            {
                IsPressed = false;
                OnRelease.Invoke();
            }


        }
    }
}
