﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CarpetCombo : MonoBehaviour
{
 
    public bool carpetplayed;
    public bool carpetrolled;
    public AudioSource VO1;
    public Animator carpet;
    public GameObject replacer;
    public GameObject hover;


    // Start is called before the first frame update
    void Start()
    {
        carpetplayed = false;
        carpetrolled = false;
        replacer.SetActive(false);
       
    }

    public void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.tag == "carpet" && carpetplayed == false)
        {
           
            VO1.Play();
            //carpet.Play();
            carpet.SetTrigger("RollOut");
            carpetplayed = true;
            Debug.Log("carpetUnrolled");
            replacer.SetActive(true);
            hover.SetActive(false);
            
        }
    }


}
