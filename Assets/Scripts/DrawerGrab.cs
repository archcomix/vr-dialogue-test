﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerGrab : MonoBehaviour
{

    public GameObject drawer;
    public Vector3 previousVector;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = drawer.transform.position;
        previousVector = transform.position;

    }

    // Update is called once per frame
    void Update()
    {
     if (previousVector != transform.position){
            drawer.transform.position = new Vector3(drawer.transform.position.x, drawer.transform.position.y, transform.position.z);
            previousVector = transform.position;
        }
    }
}

