﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CarpetRerolled: MonoBehaviour
{
 

    public bool carpetrolled;
    public AudioSource VO1;
    public Animator carpetroll;
    public GameObject phone;
    public GameObject rehover;



    // Start is called before the first frame update
    void Start()
    {
     
        carpetrolled = false;

       
    }

    public void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.tag == "carpet" && carpetrolled == false)
        {
           
            VO1.Play();
            //carpetroll.Play();
            carpetroll.SetTrigger("RollUp");
            Debug.Log("carpetrerolled");
            rehover.SetActive(false);
            phone.SetActive(true);
           
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
