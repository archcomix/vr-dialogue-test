﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderListener : MonoBehaviour {
    public bool hasTriggered;


    // Use this for initialization
    void Start() {
        hasTriggered = false;
    }

    // Update is called once per frame
    void Update() {



    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
            hasTriggered = true;

    }
}
