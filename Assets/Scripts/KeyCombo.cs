﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class KeyCombo : MonoBehaviour
{
 
    public bool keyPlayed;
    public AudioSource keyFX;
   // public float force = 1.0f;
    public GameObject keys;
    public Collider handle;


    // Start is called before the first frame update
    void Start()
    {
        keyPlayed = false;
   //     handle = handle.GetComponent<Collider>();
       handle.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Drawer Unlocked");

        if (other.gameObject.tag == "Key")
        // && keyPlayed == false
        {

            keyFX.Play();
       
           keyPlayed = true;
          
            keys.SetActive(false);
            handle.enabled = true;
        //drawer.AddForce(Vector3.up * force);
            Debug.Log("Drawer Unlocked");
            
           
        }
    }


}
