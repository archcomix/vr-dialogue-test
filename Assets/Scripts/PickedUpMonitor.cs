﻿using NodeCanvas.StateMachines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PickedUpMonitor : MonoBehaviour
{
    public bool PhonePickedUp;
    public bool KeysPickedUp;
    public bool BatteryPickedUp;
    public bool BatterySnapped;
    public bool PenRepaired;
    //public XRGrabInteractable interactable1;
    //public XRGrabInteractable interactable2;
    //public XRBaseControllerInteractor controller;
    //public bool selectTest1;
    //public bool selectTest2;
    public  GameObject experimentManager;


    // Start is called before the first frame update
    void Start()
    {
        PhonePickedUp = false;
        KeysPickedUp = false;
        BatteryPickedUp = false;
        BatterySnapped = false;
        PenRepaired = false;
        //selectTest1 = false;
        //selectTest2= false;
       


    }

    // Update is called once per frame
    //void Update()
    //{
        
    //    if (selectTest1)
    //        {
    //        selectTest1 = false;
    //        interactable1.onSelectEnter.Invoke(controller);
    //        }

    //    if (selectTest2)
    //    {
    //        selectTest2 = false;
    //        interactable2.onSelectEnter.Invoke(controller);
    //        experimentManager.GetComponent<FSMOwner>().StartBehaviour();
    //    }
    //}


    public void PhoneWasPickedUp()
    {
        PhonePickedUp = true;
       
    }

    public void KeysWerePickedUp()
    {
        KeysPickedUp = true;
    }

    public void BatteryWasPickedUp()
    {
        BatteryPickedUp = true;
    }

    public void BatteryWasSnapped()
    {
        BatterySnapped = true;
    }
   public void PenWasRepaired()
    {
        PenRepaired = true;
    }
}
