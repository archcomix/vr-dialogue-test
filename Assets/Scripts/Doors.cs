﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors: MonoBehaviour {

    private Animator _animator;
    public AudioSource audio;   

    // Use this for initialization
    void Start () {
            _animator = GetComponent<Animator>();
       
           }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") )
        {
            _animator.SetTrigger("Open");
            audio.Play();

          
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") )
        {
            _animator.SetTrigger("Close"); 
        }

    }

}
