﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashImageColour : MonoBehaviour
{
    public Gradient colour;
    public float frequency;

    private Color original;
    private Image image;
    private bool colourset = false;

    private void Awake()
    {
        image = GetComponent<Image>();   
    }

    // Start is called before the first frame update
    void Start()
    {
        original = image.color;
        colourset = true;
    }

    // Update is called once per frame
    void Update()
    {
        image.color = original * colour.Evaluate(Mathf.Sin(2 * Mathf.PI * frequency * Time.time));
    }

    private void OnDisable()
    {
        if (colourset)
        {
            image.color = original;
        }
    }
}
