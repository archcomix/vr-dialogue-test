﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeManager : MonoBehaviour
{

    public AudioSource alarm;
    public Light bedsideLight;

    // Start is called before the first frame update
    void Start()
    {
        bedsideLight = GetComponent<Light>();
        bedsideLight.enabled = !true;

      
    }


    public void LightOn()
        {
        bedsideLight.enabled = true;
        //swap out baked lit material for darkroom
    }

    // Update is called once per frame

  


    void Update()
    {
        
    }


}
