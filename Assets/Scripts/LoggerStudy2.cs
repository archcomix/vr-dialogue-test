﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LoggerStudy2 : MonoBehaviour
{

    private StreamWriter m_eventLogger;
    private StreamWriter m_telemetryLogger;

    public Transform hmd;
    //public Transform leftHand;
    //public Transform rightHand;

   public TimerScript screenTimer;

    public float logIntervals = 0.5f; // 0.5 is default

    private float m_lastLogTime = 0;

    // Use this for initialization
    void Start() {
        // Check if log folder exists and create if not
        Directory.CreateDirectory(Application.persistentDataPath + "/Logs");

        // create writers
        string timeStamp = System.DateTime.Now.ToString("yyyymmdd_hhmmss");
        m_eventLogger = new StreamWriter(Application.persistentDataPath + "/Logs/" + timeStamp + "_event.log");
        m_telemetryLogger = new StreamWriter(Application.persistentDataPath + "/Logs/" + timeStamp + "_telemetry.log");

    }

    // Update is called once per frame
    void Update() {
        // log telemetry in regular intervals
        if (m_lastLogTime + logIntervals < Time.time) {
            LogTelemetry();
            m_lastLogTime = Time.time;
        }
    }


    private void LogTelemetry() {
        string unixTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
        string timeSinceStart = screenTimer.ScreenTime().ToString();
        string timeStamp = System.DateTime.Now.ToString("hhmmss");

        Vector3 hmdPos = hmd.position;
        string hmdPosString = hmdPos.ToString();

        Vector3 hmdRot = hmd.eulerAngles;
        string hmdRotString = hmdRot.ToString();

        m_telemetryLogger.WriteLine(unixTime + "," + timeSinceStart + "," + timeStamp + "," + hmdPosString + "," + hmdRotString);
        m_telemetryLogger.Flush();
    }

    private void LogEvent(string eventIdentifier, string eventDescription) {
        string unixTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
        string timeSinceStart = screenTimer.ScreenTime().ToString();
        string timeStamp = System.DateTime.Now.ToString("hhmmss");

        m_eventLogger.WriteLine(unixTime + "," + timeSinceStart + "," + timeStamp + "," + eventIdentifier + "," + eventDescription);
        m_eventLogger.Flush();
    }

    //public void LogChange(AbstractChange change) {
    //    string eventDescription = "Change applied: " + change.ChangeName;
    //    LogEvent("CH", eventDescription);
    //}

    public void LogTaskFulfilled(Vector3 taskPos) {
        string eventDescription = "Task fulfilled at: " + taskPos.ToString();
        LogEvent("TF", eventDescription);
    }

    public void LogSpecialOrbReported()
    {
        string eventDescription = "Special Orb reported";
        LogEvent("TSO", eventDescription);
    }

    public void LogTaskScore(int score)
    {
        string eventDescription = "Current Task Score: " + score;
        LogEvent("TS", eventDescription);
    }

    public void LogCertainChangeDetected() {
        string eventDescription = "Certain Change Detected";
        LogEvent("CC", eventDescription);
    }

    public void LogCertainChangeDetectedCorrection() {
        string eventDescription = "[Correction] Certain Change Detected";
        LogEvent("CCC", eventDescription);
    }

    public void LogUncertainChangeDetected() {
        string eventDescription = "Uncertain Change Detected";
        LogEvent("UC", eventDescription);
    }

    public void LogUncertainChangeDetectedCorrection() {
        string eventDescription = "[Correction] Uncertain Change Detected";
        LogEvent("UCC", eventDescription);
    }

    public void LogStart() {
        string eventDescription = "The Experimenter Started the Experiment";
        LogEvent("ES", eventDescription);
    }

    public void LogOvertime()
    {
        string eventDescription = "The Experiment entered overtime after running out of changes.";
        LogEvent("OV", eventDescription);
    }

    public void LogEnd()
    {
        string eventDescription = "The Experiment ended";
        LogEvent("EE", eventDescription);
    }

    public void LogContinuation() {
        string eventDescription = "The Experimenter Continued the Experiment";
        LogEvent("EC", eventDescription);
    }

    public void LogSceneReset() {
        string eventDescription = "Controller Reset";
        LogEvent("CR", eventDescription);
    }

    public void LogTrainingModeStatusChange(bool status) {
        string eventDescription = "Training Mode " + (status ? "activated" : "deactivated");
        LogEvent("TM", eventDescription);
    }

    ////public void LogTransmission(RoomController.TargetRoom target) {
    ////    string eventIdentifier = "Transmission to ";

    ////    switch (target) {
    ////        case RoomController.TargetRoom.LivingRooom:
    ////            eventIdentifier = eventIdentifier + "living room";
    ////            break;
    ////        case RoomController.TargetRoom.TrainingRoom:
    ////            eventIdentifier = eventIdentifier + "training room";
    ////            break;
    ////    }

    //    LogEvent("TR", eventIdentifier);
    //}

    public void LogStroopStart()
    {
        LogEvent("SS", "Stroop Started");
    }

    public void LogStroopCorrect(float timeDiff)
    {
        string eventIdentifier = "Stroop Correct; " + timeDiff;
        LogEvent("SC", eventIdentifier);
    }

    public void LogStroopInCorrect(float timeDiff)
    {
        string eventIdentifier = "Stroop Incorrect; " + timeDiff;
        LogEvent("SI", eventIdentifier);
    }

    public void LogStroopTimeout()
    {
        string eventIdentifier = "Stroop Timeout";
        LogEvent("ST", eventIdentifier);
    }

    public void LogStroopResults(int correct, int incorrect, int timeout, float averageTimeDiff)
    {
        string eventIdentifier = "Cor: " + correct + " InC: " + incorrect + " TO: " + timeout + " avgT: " + averageTimeDiff;
        LogEvent("STR", eventIdentifier);
    }

    void OnApplicationQuit() {
        m_eventLogger.Close();
        m_telemetryLogger.Close();
    }
}
