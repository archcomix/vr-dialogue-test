﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR;

//using Valve.VR;
/*
public enum ColourNames { Orange, Green, Blue, Red, Yellow, Magenta };

public class Trial
{

    public Color TextColour { get; private set; }
    public ColourNames ColourName { get; private set; }

    public bool Matches { get; private set; }
    public float StartTime { get; private set; }

    public Trial(Color aTextColour, ColourNames aColourName)
    {
        TextColour = aTextColour;
        ColourName = aColourName;

        Matches = doesColourMatchText();
    }

    public void SetStartTime(float time)
    {
        StartTime = time;
    }

    private bool doesColourMatchText()
    {
        if (StroopModule.NameToColour[ColourName] == TextColour)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

public class StroopModule : MonoBehaviour
{
    // Colour presets
    public static readonly Color colourOrange = new Color(1.0f, 0.55f, 0.0f);
    public static readonly Color colourGreen = Color.green;
    public static readonly Color colourBlue = Color.blue;
    public static readonly Color colourRed = Color.red;
    public static readonly Color colourYellow = Color.yellow;
    public static readonly Color colourMagenta = Color.magenta;

    public static readonly Dictionary<ColourNames, Color> NameToColour =
    new Dictionary<ColourNames, Color>{ {ColourNames.Orange, colourOrange },
                                            { ColourNames.Green, colourGreen },
                                            {ColourNames.Blue, colourBlue },
                                            {ColourNames.Red, colourRed },
                                            {ColourNames.Yellow, colourYellow },
                                            {ColourNames.Magenta, colourMagenta} };

    //public static readonly Dictionary<Color, ColourNames> ColourToName =
    //    new Dictionary<Color, ColourNames>{ {colourOrange, ColourNames.Orange},
    //                                        {colourGreen, ColourNames.Green },
    //                                        {colourBlue, ColourNames.Blue},
    //                                        {colourRed, ColourNames.Red},
    //                                        {colourYellow, ColourNames.Yellow},
    //                                        {colourMagenta, ColourNames.Magenta} };

    public GameObject textObject;

    public int numberOfTrials = 10;
    public float IntervalToFail = 10;

    public LoggerStudy2 logger;

    public bool IsActive { get; private set; }

    public GameObject controllerLabelMatch;
    public GameObject controllerLabelNoMatch;

    public XRController leftController;
    public XRController rightController;

    //public GameObject tvBefore;
    //public GameObject tvStroop;

    private TextMesh m_text;
    private System.Random m_random;

    private int m_trialsDone;

    private Trial m_currentTrial;

    private int m_correct = 0;
    private int m_incorrect = 0;
    private int m_timeout = 0;

    private ColourNames m_lastColourName = ColourNames.Blue;
    private Color m_lastColour = colourBlue;

    private List<float> m_timeDiffs = new List<float>();

    private bool m_stroopStarted = false;

    // Start is called before the first frame update
    void Start()
    {

        //List<InputDevice> devices = new List<InputDevice>();
        //InputDeviceCharacteristics rightControllerCharacteristics = InputDeviceCharacteristics.Right | InputDeviceCharacteristics.Controller;
        //InputDevices.GetDevicesWithCharacteristics(rightControllerCharacteristics, devices);

        // Text object should be invisible until we say so
        textObject.SetActive(false);

        //// Same goes for the labels on the controllers and the TV
        //controllerLabelMatch.SetActive(false);
        //controllerLabelNoMatch.SetActive(false);
        //tvStroop.SetActive(false);

        m_text = textObject.GetComponent<TextMesh>();

        m_random = new System.Random();
    }

    public void StartStroop()
    {
        textObject.SetActive(true);
        controllerLabelMatch.SetActive(true);
        controllerLabelNoMatch.SetActive(true);

        //// make "real" tv disappear and stroopTV appear
        //tvBefore.SetActive(false);
        //tvStroop.SetActive(true);

        IsActive = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (IsActive)
        {
            // //   wait for the participant input to start test

            bool buttonAPressed = rightController.inputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.menuButton, out bool menu_bool);

            //   // originally: OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.Touch);

            if (buttonAPressed)
            {
                m_stroopStarted = true;

                // fade in and go stroop
                //SteamVR_Fade.Start(Color.clear, 0);
            }

            // if test is running, go stroop!
            if (m_stroopStarted)
            {
                DoStroop();
            }


            //public void DoStroopTest()
            //{
            //    Debug.Log("It fucking works!");
            //}
        }

        public void DoStroop()
        {
            // if we don't have a trial, we might want one?
            if (m_currentTrial == null)
            {
                IsActive = TryNewTrialCreation();
            }
            else
            {
                //    // User Input
                //        
                bool ControllerStateMatch = leftController.inputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.gripButton, out bool gripL_bool);
                //// originally: OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.Touch); // left



                bool ControllerStateNoMatch = rightController.inputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.gripButton, out bool gripR_bool);
                //   //originally:  OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger, OVRInput.Controller.Touch); // right


                if (ControllerStateMatch && !ControllerStateNoMatch)
                {
                    if (m_currentTrial.Matches)
                    {
                        // Count as correct
                        LogCorrect(m_currentTrial.StartTime, Time.fixedTime);
                    }
                    else
                    {
                        // Count as incorrect
                        LogIncorrect(m_currentTrial.StartTime, Time.fixedTime);
                    }

                    // check if we can get another and create if yes; deactivate if otherwise
                    IsActive = TryNewTrialCreation();
                }
                else if (!ControllerStateMatch && ControllerStateNoMatch)
                {
                    if (m_currentTrial.Matches)
                    {
                        // Count as incorrect
                        LogIncorrect(m_currentTrial.StartTime, Time.fixedTime);
                    }
                    else
                    {
                        // Count as correct
                        LogCorrect(m_currentTrial.StartTime, Time.fixedTime);
                    }

                    // check if we can get another and create if yes; deactivate if otherwise
                    IsActive = TryNewTrialCreation();
                }

                // Time
                if (Time.fixedTime > m_currentTrial.StartTime + IntervalToFail)
                {
                    // log failure
                    LogTimeout();

                    // check if we can get another and create if yes; deactivate if otherwise
                    IsActive = TryNewTrialCreation();
                }

            }

        }

        private void LogTimeout()
        {
            m_timeout++;
            logger.LogStroopTimeout();
        }

        private void LogCorrect(float startTime, float completionTime)
        {
            m_correct++;
            float timeDiff = completionTime - startTime;
            m_timeDiffs.Add(timeDiff);
            //Debug.Log("Correct - " + timeDiff + "s");
            logger.LogStroopCorrect(timeDiff);
        }

        private void LogIncorrect(float startTime, float completionTime)
        {
            m_incorrect++;
            float timeDiff = completionTime - startTime;
            m_timeDiffs.Add(timeDiff);
            //Debug.Log("Incorrect - " + timeDiff + "s");
            logger.LogStroopInCorrect(timeDiff);
        }

        private void LogResults()
        {
            float averageTimeDiff = 0;
            foreach (float diff in m_timeDiffs)
            {
                averageTimeDiff += diff;
            }
            averageTimeDiff = averageTimeDiff / m_timeDiffs.Count;

            logger.LogStroopResults(m_correct, m_incorrect, m_timeout, averageTimeDiff);

            // fade to black
            //SteamVR_Fade.Start(Color.black, 2);
        }

        private bool TryNewTrialCreation()
        {
            if (m_trialsDone < numberOfTrials)
            {
                m_trialsDone++;
                CreateNextTrial();
                return true;
            }
            else
            {
                LogResults();

                return false;
            }
        }

        private void CreateNextTrial()
        {
            m_currentTrial = GenerateRandomTrialPseudoRandom();
            m_currentTrial.SetStartTime(Time.fixedTime);

            m_text.color = m_currentTrial.TextColour;
            m_text.text = m_currentTrial.ColourName.ToString();
        }

        /// <summary>
        /// Creates Trials with complete random combinations of colour and text.
        /// However, this will lead to way more combinations who do not match.
        /// </summary>
        /// <returns></returns>
        private Trial GenerateRandomTrialFullRandom()
        {
            ColourNames cName;
            Color colour;

            // Make sure we don't get the same combination repeatedly
            do
            {
                cName = GenerateRandomColourName();
                colour = GenerateRandomTextColour();
            } while (cName == m_lastColourName && colour == m_lastColour);

            m_lastColourName = cName;
            m_lastColour = colour;

            return new Trial(colour, cName);
        }

        /// <summary>
        /// Creates Trials in a pseudorandom fashion that ensures that at least a third
        /// of the generated Trials match
        /// </summary>
        /// <returns></returns>
        private Trial GenerateRandomTrialPseudoRandom()
        {
            ColourNames cName;
            Color colour;

            // Make sure we don't get the same combination repeatedly
            do
            {
                cName = GenerateRandomColourName();

                double rand = m_random.NextDouble();

                if (rand <= 0.30)
                {
                    // colour name should be the correct one
                    colour = NameToColour[cName];
                }
                else
                {
                    colour = GenerateRandomTextColour();
                }
            } while (cName == m_lastColourName && colour == m_lastColour);

            m_lastColour = colour;
            m_lastColourName = cName;

            return new Trial(colour, cName);

        }

        private ColourNames GenerateRandomColourName()
        {
            var values = ColourNames.GetValues(typeof(ColourNames));

            return (ColourNames)values.GetValue(m_random.Next(values.Length));
        }

        private Color GenerateRandomTextColour()
        {
            int random = m_random.Next(6);

            if (random == 0)
                return colourOrange;
            else if (random == 1)
                return colourGreen;
            else if (random == 2)
                return colourBlue;
            else if (random == 3)
                return colourRed;
            else if (random == 4)
                return colourYellow;
            else
                return colourMagenta;
        }
    }
}

*/

