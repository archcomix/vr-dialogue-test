﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnityStringEvent : UnityEvent<string>
{ }

public class Keypad : MonoBehaviour
{
    private void Awake()
    {
        OnClear = new UnityEvent();
        OnBack = new UnityEvent();
        OnDigit = new UnityStringEvent();
        KeypadValue  = "";
             
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (var button in GetComponentsInChildren<KeypadButton>())
        {
            button.OnClick.AddListener(new UnityAction<string>(OnKeyPadButton));
        }

        OnDigit.AddListener((DigitValue) =>
        {
            KeypadValue += DigitValue;
        });


        OnBack.AddListener(() =>
        {
            KeypadValue.Remove(KeypadValue.Length-1) ;
        });

        OnClear.AddListener(() =>
        {
            KeypadValue = "" ;
        });


    }

    void OnKeyPadButton(string buttonvalue)
    {
        switch (buttonvalue)
        {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                OnDigit.Invoke(buttonvalue);
                break;
            case "Clear":
                OnClear.Invoke();
                break;
            case "Back":
                OnBack.Invoke();
                break;
        }
    }

 public string KeypadValue;

    [HideInInspector]
    public UnityStringEvent OnDigit;
    [HideInInspector]
    public UnityEvent OnClear;
    [HideInInspector]
    public UnityEvent OnBack;

    // Update is called once per frame
    void Update()
    {
        
    }
}
