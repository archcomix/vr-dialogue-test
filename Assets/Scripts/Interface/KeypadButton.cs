﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeypadButton : MonoBehaviour
{
    protected string button;

    public UnityStringEvent OnClick;

    public void Awake()
    {
        OnClick = new UnityStringEvent();
        button = GetComponentInChildren<Text>().text;
        GetComponent<Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(
            () =>
            {
                OnClick.Invoke(button);
            }));
    }

    public void OnTap()
    {
        GetComponent<Button>().onClick.Invoke();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
