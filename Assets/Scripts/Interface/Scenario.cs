﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Events;

public class Scenario : Questionaire
{
    [Header("Scenario")]
    public Text mResponseOutput;
    public GameObject mResponseParent;

    [Serializable]
    public class QuestionAsked : UnityEvent<string, AudioClip> { }
    public QuestionAsked OnQuestionAsked = new QuestionAsked();

    [Serializable]
    public class ScenarioComplete : UnityEvent { }
    public ScenarioComplete OnScenarioComplete = new ScenarioComplete();

    public ScenarioNodeData mStartingNode = null;

    private ScenarioNode mCurrentNode = null;

    private void Start()
    {
        mCurrentNode = mStartingNode.node;
        StartNode();
    }

    // Plays prompt, and responses from NPC
    private IEnumerator PlayResponse(AnswerButton button)
    {
        // Disable canvas
        mCanvas.SetActive(false);

        // We will re-use this event to output response as well
        AnswerPair answerPair = button.mPairs[button.mQuestionIndex];
        Output(answerPair.response, answerPair.animationTrigger, answerPair.voiceClip);

        if (mResponseParent)
        {
            mResponseOutput.text = answerPair.response;
            mResponseParent.gameObject.SetActive(true);
        }

        // Wait for audio to complete
        float waitTime = 1.5f;
        if (answerPair.voiceClip)
            waitTime += answerPair.voiceClip.length;

        // Wait for audio
        yield return new WaitForSeconds(waitTime);

        // Check if answer hook
        AnswerHook answerHook = answerPair as AnswerHook;
        if (answerHook != null)
        {
            // If we have a hook, check if it has a node to go to
            CheckForNode(answerHook);
        }
        else
        {
            // If not answer hook now, it will be next
            FillButtons(mCurrentNode.answerHooks);

            if (mQuestionOutput)
                mQuestionOutput.text = mCurrentNode.question;
        }

        // Enable canvas
        mCanvas.SetActive(true);

        if (mResponseParent)
        {
            mResponseParent.gameObject.SetActive(false);
            mResponseOutput.text = "";
        }
    }

    // Check if node hook
    private void CheckForNode(AnswerHook answerHook)
    {
        // If no hook, end scenario
        if (answerHook.node == null)
        {
            if (OnScenarioComplete != null)
            {
                mCanvas.GetComponent<Canvas>().enabled = false;

                OnScenarioComplete.Invoke();
                return;
            }
        }

        // If there is a node, set it
        mCurrentNode = answerHook.node.node;

        // Start new node
        StartNode();
    }

    // This outputs the node data to the scene/character
    private void Output(string subtitle, string trigger, AudioClip voiceClip)
    {
        // For the scenario, when a question is asked, we want to play an animation
        if (OnQuestionAsked != null)
            OnQuestionAsked.Invoke(trigger, voiceClip);
    }

    // This fills the answer buttons
    private void FillButtons(AnswerPair[] answerPairs)
    {
        for (int i = 0; i < mButtons.Count; i++)
            mButtons[i].Fill(answerPairs, answerPairs[i].answer, i);
    }

    // This outputs a new node's opening question, animation, and voice
    private void StartNode()
    {
        Output(mCurrentNode.question, mCurrentNode.animationTrigger, mCurrentNode.voiceClip);
        FillButtons(mCurrentNode.answerPairs);

        if (mQuestionOutput)
            mQuestionOutput.text = mCurrentNode.additionalQuestion;
    }

    // Called by button
    public void Answer(AnswerButton button)
    {
        StartCoroutine(PlayResponse(button));
    }
}
