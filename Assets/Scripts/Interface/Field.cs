﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Field : MonoBehaviour
{
    public Keypad keypad;

    // Start is called before the first frame update
    void Start()
    {
        var inputfield = GetComponent<InputField>();
        keypad.OnDigit.AddListener(new UnityAction<string>(s => { inputfield.text += s; }));
        keypad.OnClear.AddListener(new UnityAction(() => { inputfield.text = ""; }));
        keypad.OnBack.AddListener(new UnityAction(() => { inputfield.text = inputfield.text.Substring(0, inputfield.text.Length - 1); }));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
