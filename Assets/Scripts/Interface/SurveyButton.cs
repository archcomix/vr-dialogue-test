﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SurveyButton : MonoBehaviour
{
    public Text mOutput;

    public void Fill(string question)
    {
        mOutput.text = question;
    }
}
