﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]
public class ButtonSounds : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
{
    public AudioClip mClickClip;
    public AudioClip mEnterClip;

    private AudioSource mAudioSource;

    private void Awake()
    {
        mAudioSource = GetComponent<AudioSource>();
    }

    public void OnPointerClick(PointerEventData data)
    {
        mAudioSource.PlayOneShot(mClickClip);
    }

    public void OnPointerEnter(PointerEventData data)
    {
        mAudioSource.PlayOneShot(mEnterClip);
    }
}
