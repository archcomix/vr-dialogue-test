﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.DialogueTrees;

public class AnswerButton : MonoBehaviour
{
    public Text mOutput;
    public int mQuestionIndex = 0;
    public AnswerPair[] mPairs = null;
    public KeyCode mKeyCode;

    private Text textComponent;

    public event Action<int> OnClick;

    public string Text
    {
        set
        {
            textComponent.text = value;
        }
    }

    public void SetOnclick(Action<int> callback)
    {
        OnClick = callback;
    }

    private void Awake()
    {
        textComponent = GetComponentInChildren<Text>();
        GetComponent<Button>().onClick.AddListener(
            new UnityEngine.Events.UnityAction(()=> 
            {
                if(OnClick != null)
                    OnClick(mQuestionIndex);
            }));
    }

    public string Meta
    {
        set
        {
            switch (value)
            {
                case "Flash":
                    try
                    {
                        GetComponent<FlashImageColour>().enabled = true;
                    }
                    catch
                    {
                    }
                    break;
            }
        }
    }


    private void Update()
    {
        if(Input.GetKeyDown(mKeyCode))
        {
            GetComponent<Button>().onClick.Invoke();
        }
    }

    public void Fill(AnswerPair[] pairs, string question, int index)
    {
        mPairs = pairs;
        mOutput.text = question;
        mQuestionIndex = index;
    }

    private void OnDisable()
    {
        try
        {
            GetComponent<FlashImageColour>().enabled = false;
        }
        catch
        {
        }
    }
}
