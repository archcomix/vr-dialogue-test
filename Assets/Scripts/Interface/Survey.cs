﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Survey : Questionaire
{
    [Header("Survey")]
    public bool mDisableOnComplete = false;

    [Serializable]
    public class QuestionsComplete : UnityEvent<Answer> { }
    public QuestionsComplete OnQuestionsComplete = new QuestionsComplete();

    private bool mSurveyCompleted = false;
    private SurveyData mSurveyData = null;
    private Answer mAnswers = new Answer();
    private List<SurveyQuestion> mSurveyQuestions = null;

    private void Awake()
    {
        mSurveyData = (SurveyData)Resources.Load("SurveyData");
        mSurveyQuestions = mSurveyData.mQuestions;

        Output(mSurveyQuestions[mIndex]);
    }

    private void OnDisable()
    {
        if (!mSurveyCompleted) OnQuestionsComplete.Invoke(mAnswers);
    }

    protected override void Output(Question newQuestion)
    {
        // Cast
        SurveyQuestion question = (SurveyQuestion)newQuestion;

        // Show answers
        for (int i = 0; i < mButtons.Count; i++)
            mButtons[i].Fill(null, question.answers[i], i);

        // Base
        base.Output(newQuestion);
    }

    protected override bool NextQuestion(AnswerButton button, List<Question> questions)
    {
        // Save previous
        if (mSurveyQuestions[mIndex].OnQuestionAnswered != null)
            mSurveyQuestions[mIndex].OnQuestionAnswered.Invoke(mAnswers, button.mOutput.text);

        // Call other functionality, if no more questions, return false
        if (!base.NextQuestion(button, questions))
        {
            mSurveyCompleted = true;
            OnQuestionsComplete.Invoke(mAnswers);

            mCanvas.GetComponent<Canvas>().enabled = false;
        }

        return true;
    }

    public void NextWrapper(AnswerButton button)
    {
        NextQuestion(button, mSurveyQuestions.Cast<Question>().ToList());
    }
}
