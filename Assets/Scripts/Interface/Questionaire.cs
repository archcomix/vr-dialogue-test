﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Questionaire : MonoBehaviour
{
    [Header("Scene Objects")]
    public Text mQuestionOutput;
    public GameObject mCanvas;
    public List<AnswerButton> mButtons;

    protected int mIndex = 0;

    protected virtual void Output(Question newQuestion)
    {
        // Output question
        mQuestionOutput.text = newQuestion.question;
    }

    protected virtual bool NextQuestion(AnswerButton button, List<Question> questions)
    {
        // Default 
        string answer = "";

        // If button passed in, replace
        if (button)
            answer = button.mOutput.text;

        // Next
        mIndex++;

        // If last, close survey
        if (mIndex >= questions.Count)
        {
            return false;
        }

        // Output
        Question question = questions[mIndex];
        Output(question);

        return true;
    }
}
