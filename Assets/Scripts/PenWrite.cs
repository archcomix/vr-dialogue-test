﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PenWrite : MonoBehaviour
{
    public GameObject trail;
    public GameObject currentTrail;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTrigger(Collider Paper)
    {
        //will update position only when it's colliding
        if (currentTrail != null)
        {
            currentTrail.transform.position = transform.position;
        }
    }

void OnTriggerEnter (Collider Paper)
    {
        if (Paper.tag == "Paper")
        {
            currentTrail = Instantiate(trail);
        }
    }

    void OnTriggerExit (Collider Paper)
    {
        currentTrail = null;
    }

}

