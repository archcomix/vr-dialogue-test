﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Measurements : MonoBehaviour
{
    public static Measurements instance;

    protected virtual void Awake()
    {
        intervals = new Dictionary<string, Interval>();
        questionnaireResponses = new List<KeyValuePair<string, string>>();
        instance = this;
        prefix = "";
    }

    public class Interval
    {
        public float start;
        public float end;

        public float length
        {
            get
            {
                return end - start;
            }
        }
    }

    protected Dictionary<string, Interval> intervals;
    protected List<KeyValuePair<string, string>> questionnaireResponses;
    protected string sessionid;
    protected string prefix;

    public static void SetPrefix(string prefix)
    {
        instance.prefix = prefix;
    }

    public static void BeginInterval(string id)
    {
        id = instance.prefix + "_" + id;
        if (!instance.intervals.ContainsKey(id))
        {
            instance.intervals.Add(id, new Interval());
        }
        instance.intervals[id].start = Time.time;
    }

    public static void EndInterval(string id)
    {
        try
        {
            id = instance.prefix + "_" + id;
            instance.intervals[id].end = Time.time;
        }catch(KeyNotFoundException e)
        {
            Debug.LogWarning("Tried to stop non-started timer " + id);
        }
    }

    public static void QuestionnaireResponse(string field, string answer)
    {
        field = instance.prefix + "_" + field;
        instance.questionnaireResponses.Add(new KeyValuePair<string, string>(field, answer));
    }

    public static void SetSessionId(string id)
    {
        instance.sessionid = id;
    }
}
