﻿using NodeCanvas.Framework;
using ParadoxNotion;
using ParadoxNotion.Design;
using UnityEngine;


namespace NodeCanvas.DialogueTrees
{

    [Name("DEFAULT NODE")]
    [Category("Control")]
    [Description("Catches the Dialogue Tree control flow if it exits a node with no connections.")]
    [Icon("Filter")]
    [Color("00b9e8")]
    public class DefaultDialogueNode : DTNode
    {
        public override int maxOutConnections { get { return 1; } }
        public override bool requireActorSelection { get { return false; } }

        protected override Status OnExecute(Component agent, IBlackboard bb)
        {
            DLGTree.Continue(0);
            return status;
        }

        ////////////////////////////////////////
        ///////////GUI AND EDITOR STUFF/////////
        ////////////////////////////////////////
#if UNITY_EDITOR

        protected override void OnNodeInspectorGUI()
        {
            DrawDefaultInspector();
        }

#endif
    }
}