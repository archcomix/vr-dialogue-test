﻿using NodeCanvas.DialogueTrees;
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The Avatar Say subclass behaves similarly to the the Say Statement Node, except that the Metadata is forced to be one of the Trigger values in the attached AnimatorController
/// </summary>
[Name("Avatar Say")]
[Description("Make the selected Avatar Dialogue Actor talk. You can make the text more dynamic by using variable names in square brackets\ne.g. [myVarName] or [Global/myVarName]")]
public class AvatarStatementDialogueNode : DTNode
{
    [SerializeField]
    protected Statement statement = new Statement("This is a dialogue text");

    public override bool requireActorSelection { get { return true; } }

    protected override Status OnExecute(Component agent, IBlackboard bb)
    {
        var tempStatement = statement.BlackboardReplace(bb);
        DialogueTree.RequestSubtitles(new SubtitlesRequestInfo(finalActor, tempStatement, OnStatementFinish));
        return Status.Running;
    }

    void OnStatementFinish()
    {
        status = Status.Success;
        DLGTree.Continue();
    }

    ////////////////////////////////////////
    ///////////GUI AND EDITOR STUFF/////////
    ////////////////////////////////////////
#if UNITY_EDITOR

    protected override void OnNodeGUI()
    {
        var displayText = statement.text.Length > 30 ? statement.text.Substring(0, 30) + "..." : statement.text;
        GUILayout.Label("\"<i> " + displayText + "</i> \"");
    }

    protected override void OnNodeInspectorGUI()
    {
        base.OnNodeInspectorGUI();
        var areaStyle = new GUIStyle(GUI.skin.GetStyle("TextArea"));
        areaStyle.wordWrap = true;

        GUILayout.Label("Dialogue Text");
        statement.text = UnityEditor.EditorGUILayout.TextArea(statement.text, areaStyle, GUILayout.Height(100));
        statement.audio = UnityEditor.EditorGUILayout.ObjectField("Audio File", statement.audio, typeof(AudioClip), false) as AudioClip;

        var actor = finalActor as AvatarDialogueActor;
        if (actor == null)
        {
            UnityEditor.EditorGUILayout.LabelField("Please select an AvatarDialogueActor as the Actor Parameter");
        }
        else
        {
            var animator = (finalActor as AvatarDialogueActor).gameObject.GetComponent<Animator>();
            if (animator == null)
            {
                UnityEditor.EditorGUILayout.LabelField("Please add an Animator to the AvatarDialogueController");
            }
            else
            {
                var controller = (animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController);
                if (controller == null)
                {
                    UnityEditor.EditorGUILayout.LabelField("Please add an Animator to the AvatarDialogueController");
                }else
                {
                    var parameters = controller.parameters.Select(p => p.name).ToList();
                    statement.meta = EditorUtils.StringPopup(statement.meta, parameters, true, false);                   
                }
            }
        }    
    }

#endif
}
