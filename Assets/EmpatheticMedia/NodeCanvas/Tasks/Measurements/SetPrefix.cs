﻿using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace NodeCanvas.Tasks.Actions
{
    [Category("Empathetic Media")]
    [Description("Sets the Measurements Prefix")]
    public class SetPrefix : ActionTask
    {
        public string Prefix;

        protected override void OnExecute()
        {
            Measurements.SetPrefix(Prefix);
            EndAction();
        }

        protected override string info
        {
            get { return string.Format("{0} <i>{1}</i>", "Set Prefix to", Prefix); }
        }

    }
}