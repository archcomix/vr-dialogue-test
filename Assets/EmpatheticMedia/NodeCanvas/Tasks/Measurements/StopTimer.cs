﻿using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace NodeCanvas.Tasks.Actions
{
    [Category("Empathetic Media")]
    [Description("Stops a Timer with the Interval Name")]
    public class StopTimer : ActionTask
    {
        public string MeasurementName;

        protected override void OnExecute()
        {
            Measurements.EndInterval(MeasurementName);
            EndAction();
        }
    }
}