﻿using UnityEngine;
using ParadoxNotion.Design;
using NodeCanvas.Framework;
using NodeCanvas.DialogueTrees;

namespace EmpatheticMedia.NodeCanvas.Tasks.Actions
{

    [Category("Empathetic Media")]
    [Description("Starts the Dialogue Tree assigned on a Dialogue Tree Controller object with specified agent used for 'Instigator'.")]
    [Icon("Dialogue")]
    public class StartDialogue : ActionTask<DialogueActor>
    {
        [RequiredField]
        public BBParameter<DialogueTreeController> dialogueTreeController;
        public bool waitActionFinish = true;
        public bool activateActor = true;
        public bool restoreActivationState = true;

        private bool activationState;

        protected override string info
        {
            get { return string.Format("Start Dialogue {0}", dialogueTreeController); }
        }

        protected override void OnExecute()
        {
            activationState = agent.isActiveAndEnabled;

            if (activateActor)
            {
                agent.gameObject.SetActive(true);
            }

            if (waitActionFinish)
            {
                dialogueTreeController.value.StartDialogue(agent, End);
            }
            else
            {
                dialogueTreeController.value.StartDialogue(agent);
                End(true);
            }
        }

        protected void End(bool success)
        {
            if(restoreActivationState)
            {
                agent.gameObject.SetActive(activationState);
            }

            EndAction(success);
        }
    }
}