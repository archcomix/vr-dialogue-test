﻿using System.Collections.Generic;
using UnityEngine;
using ParadoxNotion.Design;
using NodeCanvas.Framework;
using NodeCanvas.DialogueTrees;

namespace EmpatheticMedia.NodeCanvas.Tasks.Actions
{

    [Category("Empathetic Media")]
    [Description("Displays a single message to the user and waits for a response.")]
    [Icon("Dialogue")]
    public class ModalDialogueMessage : ActionTask<DialogueActor>
    {
        public string statement;
        public string response;
        public bool activateActor = true;
        public bool restoreActivationState = true;

        private bool activationState;
        private Dictionary<IStatement, int> options;

        protected override string info
        {
            get { return string.Format("Say <i>" + statement.Substring(0, 15) + "</i> modal."); }
        }

        protected override string OnInit()
        {
            options = new Dictionary<IStatement, int>();
            options.Add(new Statement(response, null, null), 0);
            return base.OnInit();
        }

        protected override void OnExecute()
        {
            activationState = agent.isActiveAndEnabled;

            if (activateActor)
            {
                agent.gameObject.SetActive(true);
            }

            var subtitlesinfo = new SubtitlesRequestInfo(agent, new Statement(statement, null), SubtitlesCallback);
            DialogueTree.RequestSubtitles(subtitlesinfo);
        }

        protected void SubtitlesCallback()
        {
            var choicesinfo = new MultipleChoiceRequestInfo(agent, options, 0, ChoiceCallback);
            DialogueTree.RequestMultipleChoices(choicesinfo);
        }

        protected void ChoiceCallback(int i)
        {
            if (restoreActivationState)
            {
                agent.gameObject.SetActive(activationState);
            }

            EndAction();
        }
    }
}