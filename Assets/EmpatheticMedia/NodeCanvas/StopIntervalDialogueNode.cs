﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.DialogueTrees;
using ParadoxNotion.Design;
using NodeCanvas.Framework;

[Name("Stop Timer")]
[Category("Empathetic Media")]
[Description("End measuring the time between this node and the equivalent Start node.")]
public class StopIntervalDialogueNode : DTNode
{
    public override bool requireActorSelection => false;

    public string MeasurementName;

    protected override Status OnExecute(Component agent, IBlackboard blackboard)
    {
        Measurements.EndInterval(MeasurementName);
        status = Status.Success;
        DLGTree.Continue();
        return status;
    }

#if UNITY_EDITOR

    protected override void OnNodeGUI()
    {
        GUILayout.Label(MeasurementName);
    }

#endif
}
