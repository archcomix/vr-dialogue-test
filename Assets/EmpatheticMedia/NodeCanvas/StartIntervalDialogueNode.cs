﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.DialogueTrees;
using ParadoxNotion.Design;
using NodeCanvas.Framework;

[Name("Start Timer")]
[Category("Empathetic Media")]
[Description("Begin to measure the time between this node and the equivalent Stop node.")]
public class StartIntervalDialogueNode : DTNode
{
    public override bool requireActorSelection => false;

    public string MeasurementName;

    protected override Status OnExecute(Component agent, IBlackboard blackboard)
    {
        Measurements.BeginInterval(MeasurementName);
        status = Status.Success;
        DLGTree.Continue();
        return status;
    }

#if UNITY_EDITOR

    protected override void OnNodeGUI()
    {
        GUILayout.Label(MeasurementName);
    }

#endif
}
