﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.DialogueTrees;
using ParadoxNotion.Design;
using NodeCanvas.Framework;

[Icon("List")]
[Name("Questionnaire Multiple Choice")]
[Category("Empathetic Media")]
[Description("Prompt a Questionnaire Multiple Choice. The Question can either be stated by a Say node or specified in this Node. On answering, the Node emits an Answer Event.")]
[Color("b3ff7f")]
public class QuestionnaireMultipleChoiceNode : MultipleChoiceNode
{
    /// <summary>
    /// If true, this Node will emit a RequestSubtitles callback with the text in the Question field. Otherwise, the Node will assume a Statement Node has asked the question.
    /// </summary>
    public bool sayQuestion;
    [TextAreaField(4)]
    public string question;
    public string fieldName;

    protected override Status OnExecute(Component agent, IBlackboard bb)
    {
        System.Action Continue = () => { base.OnExecute(agent, bb); };

        if (sayQuestion)
        {
            var speechInfo = new SubtitlesRequestInfo(finalActor, new Statement(question), Continue);
            DialogueTree.RequestSubtitles(speechInfo);
        }
        else
        {
            Continue();
        }

        return Status.Running;
    }

    public override void OnOptionSelected(int index)
    {
        var answer = availableChoices[index].statement.text;

        Measurements.QuestionnaireResponse(fieldName, index.ToString());

        base.OnOptionSelected(index);
    }

#if UNITY_EDITOR
    protected override void OnNodeGUI()
    {
        GUILayout.Label(fieldName);

        var displayText = question.Length > 30 ? question.Substring(0, 30) + "..." : question;
        GUILayout.Label("\"<i> " + displayText + "</i> \"");

        base.OnNodeGUI();
    }
#endif
}
