﻿using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using UnityEngine;


namespace NodeCanvas.DialogueTrees
{

    [Icon("Condition")]
    [Name("Wait Condition")]
    [Category("Branch")]
    [Description("Wait for the Condition to be true before proceeding.")]
    [Color("b3ff7f")]
    public class WaitConditionNode : DTNode, ITaskAssignable<ConditionTask>
    {
        [SerializeField]
        private ConditionTask _condition;

        public ConditionTask condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        public Task task
        {
            get { return condition; }
            set { condition = (ConditionTask)value; }
        }

        public override int maxOutConnections { get { return 1; } }
        public override bool requireActorSelection { get { return true; } }

        protected override Status OnExecute(Component agent, IBlackboard bb)
        {
            if (outConnections.Count == 0)
            {
                return Error("There are no connections on the Dialogue Condition Node");
            }

            if (condition == null)
            {
                return Error("There is no Conidition on the Dialoge Condition Node");
            }

            StartCoroutine(checkCondition());

            return Status.Running;
        }

        private IEnumerator checkCondition()
        {
            while(true)
            {
                if(condition.CheckCondition(finalActor.transform, graphBlackboard))
                {
                    break;
                }
                yield return null;
            }

            SetStatus(Status.Success);
            DLGTree.Continue();
        }

        ////////////////////////////////////////
        ///////////GUI AND EDITOR STUFF/////////
        ////////////////////////////////////////
#if UNITY_EDITOR

        public override string GetConnectionInfo(int i)
        {
            return i == 0 ? "Then" : "Else";
        }

        protected override void OnNodeGUI()
        {
            if (outConnections.Count == 0)
            {
                GUILayout.Label("No Outcomes Connected");
            }
        }

#endif
    }
}