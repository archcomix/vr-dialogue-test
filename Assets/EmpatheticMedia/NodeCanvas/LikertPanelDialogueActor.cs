﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.DialogueTrees;


/// <summary>
/// Implements an Actor controlling a single panel for multiple-choice questionnaire responses. This component receives both subtitle and response requests.
/// </summary>
public class LikertPanelDialogueActor : DialogueActor, IDialogueActor
{
    [HideInInspector]
    public new Texture2D portrait => null;
    [HideInInspector]
    public new Sprite portraitSprite => null;
    [HideInInspector]
    public new Color dialogueColor => Color.white;
    [HideInInspector]
    public new Vector3 dialoguePosition => transform.position;

    private AudioSource audioSource;

    protected MultipleChoiceRequestInfo request;

    public Text questionField;
    public Button continueButton;
    public LikertScaleController likertScale;

    private void Reset()
    {
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        continueButton.onClick.AddListener(new UnityEngine.Events.UnityAction(continueButton_OnClick));
        likertScale.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        DialogueTree.OnMultipleChoiceRequest += DialogueTree_OnMultipleChoiceRequest;
        DialogueTree.OnSubtitlesRequest += DialogueTree_OnSubtitlesRequest;
    }

    private void OnDisable()
    {
        DialogueTree.OnMultipleChoiceRequest -= DialogueTree_OnMultipleChoiceRequest;
        DialogueTree.OnSubtitlesRequest -= DialogueTree_OnSubtitlesRequest;
    }

    private void DialogueTree_OnSubtitlesRequest(SubtitlesRequestInfo obj)
    {
        if (obj.actor.transform != transform)
        {
            return;
        }

        likertScale.gameObject.SetActive(true);

        questionField.text = obj.statement.text;

        if (obj.statement.audio != null && audioSource != null)
        {
            audioSource.PlayOneShot(obj.statement.audio);
        }

        obj.Continue();
    }

    private void DialogueTree_OnMultipleChoiceRequest(MultipleChoiceRequestInfo obj)
    {
        if (obj.actor.transform != transform)
        {
            return;
        }

        // ensure we have enough buttons

        likertScale.SetScaleSize(obj.options.Count - 1);
        likertScale.SetMinLabel(obj.options.First().Key.text);
        likertScale.SetMaxLabel(obj.options.Last().Key.text);

        request = obj;
    }

    private void continueButton_OnClick()
    {
        likertScale.gameObject.SetActive(false);
        request.SelectOption(likertScale.Value);
        likertScale.ResetValue();
    }

}
