﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.DialogueTrees;

public class AvatarDialogueActor : DialogueActor
{
    private Animator animator;
    private AudioSource source;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        DialogueTree.OnSubtitlesRequest += DialogueTree_OnSubtitlesRequest;
    }

    private void OnDisable()
    {
        DialogueTree.OnSubtitlesRequest -= DialogueTree_OnSubtitlesRequest;
    }

    private void DialogueTree_OnSubtitlesRequest(SubtitlesRequestInfo obj)
    {
        if(obj.actor.transform != transform)
        {
            return;
        }

        float waitTime = 0.5f;

        if (obj.statement.audio != null)
        {
            source.clip = obj.statement.audio;
            source.Play();
            waitTime += obj.statement.audio.length;
        }

        animator.SetTrigger(obj.statement.meta);

        StartCoroutine(WaitForAction(waitTime, obj.Continue));
    }

    IEnumerator WaitForAction(float seconds, Action action)
    {
        var timer = 0f;
        while(timer < seconds)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        action();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
