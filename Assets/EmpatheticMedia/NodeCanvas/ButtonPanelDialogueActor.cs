﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.DialogueTrees;


/// <summary>
/// Implements an Actor controlling a single panel for multiple-choice questionnaire responses. This component receives both subtitle and response requests.
/// </summary>
public class ButtonPanelDialogueActor : DialogueActor, IDialogueActor
{
    [HideInInspector]
    public new Texture2D portrait => null;
    [HideInInspector]
    public new Sprite portraitSprite => null;
    [HideInInspector]
    public new Color dialogueColor => Color.white;
    [HideInInspector]
    public new Vector3 dialoguePosition => transform.position;

    private List<AnswerButton> answerButtons;
    private AudioSource audioSource;
    

    protected MultipleChoiceRequestInfo request;

    public Text questionField;
    public GameObject buttonPrototype;
    public Transform buttonsParent;

    private void Reset()
    {
        buttonsParent = GetComponentInChildren<HorizontalOrVerticalLayoutGroup>().transform;
        buttonPrototype = GetComponentInChildren<AnswerButton>().gameObject;
    }

    private void Awake()
    {
        answerButtons = GetComponentsInChildren<AnswerButton>().ToList();
        audioSource = GetComponent<AudioSource>();

        // disable buttons to start with to avoid us having to remember to do in scene graph. 
        // do this in awake because Start() may be called *after* a dialogue tree's BeginDialogue call, if the DialogueTree is called first thing.

        foreach (var button in answerButtons)  
        {
            button.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
    }

    private void OnEnable()
    {
        DialogueTree.OnMultipleChoiceRequest += DialogueTree_OnMultipleChoiceRequest;
        DialogueTree.OnSubtitlesRequest += DialogueTree_OnSubtitlesRequest;
    }

    private void OnDisable()
    {
        DialogueTree.OnMultipleChoiceRequest -= DialogueTree_OnMultipleChoiceRequest;
        DialogueTree.OnSubtitlesRequest -= DialogueTree_OnSubtitlesRequest;
    }

    private void DialogueTree_OnSubtitlesRequest(SubtitlesRequestInfo obj)
    {
        if (obj.actor.transform != transform)
        {
            return;
        }

        questionField.text = obj.statement.text;

        if(obj.statement.audio != null && audioSource != null)
        {
            audioSource.PlayOneShot(obj.statement.audio);
        }

        obj.Continue();
    }

    private void DialogueTree_OnMultipleChoiceRequest(MultipleChoiceRequestInfo obj)
    {
        if(obj.actor.transform != transform)
        {
            return;
        }

        // ensure we have enough buttons

        buttonPrototype.SetActive(true);

        while(answerButtons.Count < obj.options.Count)
        {
            GameObject.Instantiate(buttonPrototype, buttonsParent);
            answerButtons = GetComponentsInChildren<AnswerButton>(true).ToList();
        }

        foreach (var button in answerButtons)
        {
            button.gameObject.SetActive(false);
            button.SetOnclick(Button_OnClick);
        }

        foreach (var option in obj.options)
        {
            answerButtons[option.Value].Text = option.Key.text;
            answerButtons[option.Value].mQuestionIndex = option.Value;
            answerButtons[option.Value].gameObject.SetActive(true);
            answerButtons[option.Value].Meta = option.Key.meta;
        }

        request = obj;
    }

    private void Button_OnClick(int buttonIndex)
    {
        foreach (var button in answerButtons)   // disable first so SelectOption will re-enable the necessary (if any) next ones
        {
            button.gameObject.SetActive(false);
        }

        if (request != null)
        { 
            request.SelectOption(buttonIndex);
        }
    }

}
